À la clinique d’acupuncture familiale Verdun, nous avons à cœur la santé globale, c’est pourquoi nous  vous offrons, dans un espace paisible, plusieurs autres avenues thérapeutiques  telles l’ostéopathie, la massothérapie, l’Ayurveda etc.

Address: 5057 Wellington St, Verdun, QC H4G 1Y1, Canada

Phone: 514-582-2055